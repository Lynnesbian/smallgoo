#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QFutureWatcher>
#include <QMainWindow>
#include <QSettings>

#include <imageinfo.h>
#include <optimiser.h>

#include "SQLiteCpp/Database.h"

QT_BEGIN_NAMESPACE
namespace Ui {
	class MainWindow;
}
QT_END_NAMESPACE

class MainWindow: public QMainWindow {
Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = nullptr);

	~MainWindow();

	static SQLite::Database connectToDatabase();

private:
	static void updateImageInfo(ImageInfo &image);

	static bool optimiseImage(const ImageInfo &image, const Optimiser &optimiser);

	void loadSettings();


signals:
#pragma clang diagnostic push
#pragma ide diagnostic ignored "NotImplementedFunctions"

	void broadcastStatusUpdate(const QString &text);

#pragma clang diagnostic pop

private slots:

	void showSettingsWindow();

	void selectDirectory();

	void directorySelected(const QString &dir);

	void begin();

	void initialHashingComplete();

	void optimisingComplete();

	void finalHashingComplete();

	void handleStatusUpdate(const QString &text);

	bool checkForCancellation();

	void handleCancellation();

private:
	Ui::MainWindow *ui;
	QVector<ImageInfo> images;
	QFutureWatcher<void> watcher;
	QVector<Optimiser> optimisers;
	long double image_size;

	void resetState(bool clear_status);

	void initialiseOptimisers();
};

#endif // MAINWINDOW_H
