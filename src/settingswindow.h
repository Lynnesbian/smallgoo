#ifndef SETTINGSWINDOW_H
#define SETTINGSWINDOW_H

#include <QDialog>
#include <QSettings>
#include <QAbstractButton>

namespace Ui {
	class SettingsWindow;
}

class SettingsWindow: public QDialog {
Q_OBJECT

public:
	explicit SettingsWindow(QWidget *parent);

	~SettingsWindow();

private slots:

	static void showAboutDialogue();

	static void clearDatabase();

	void handleButtonBox(QAbstractButton *button);

private:
	Ui::SettingsWindow *ui;

	void saveSettings();
};

#endif // SETTINGSWINDOW_H
