#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCUnusedGlobalDeclarationInspection"

// other project headers
#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "settingswindow.h"
#include "settings.h"

// qt
#include <QMessageBox>
#include <QFileDialog>
#include <QDirIterator>
#include <QtConcurrent>

// dependencies
#include <xxhash.hpp>
#include <fplus/fplus.hpp>
#include <matchit.h>
#include <fmt/core.h>

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent), ui(new Ui::MainWindow) {

	ui->setupUi(this);
	initialiseOptimisers();

	auto db = connectToDatabase();

	// debug
	QStringList ok = {"Available optimisers: "};
	QStringList err = {"Unavailable optimisers: "};
	for (const auto &o: optimisers) {
		auto out = o.name + " [." + o.exts.join(", .") + "]";
		if (o.available) {
			ok << out;
		} else {
			err << out;
		}
	}
	qInfo() << ok.join(", ");
	qInfo() << err.join(", ");

	// connect signals to relevant slots
	connect(&watcher, &QFutureWatcher<void>::progressValueChanged, MainWindow::ui->progressBar, &QProgressBar::setValue);
	connect(this, &MainWindow::broadcastStatusUpdate, this, &MainWindow::handleStatusUpdate);
	connect(ui->settingsButton, &QAbstractButton::clicked, this, &MainWindow::showSettingsWindow);
	connect(ui->dirButton, &QAbstractButton::clicked, this, &MainWindow::selectDirectory);
	connect(ui->dirTxt, &QLineEdit::textChanged, this, &MainWindow::directorySelected);

	// load settings
	loadSettings();

	// reset UI and internal state
	resetState(true);
	image_size = 0;
}

MainWindow::~MainWindow() {
	delete ui;
}

void MainWindow::showSettingsWindow() {
	auto settingsWindow = SettingsWindow(this);
	settingsWindow.exec();
	// initialiseOptimisers();
	loadSettings();
}

void MainWindow::selectDirectory() {
	QString dir = QFileDialog::getExistingDirectory(
		this, "Open Directory", QDir::homePath(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks
	);
	qInfo() << dir;
	MainWindow::ui->dirTxt->setText(dir);
}

void MainWindow::directorySelected(const QString &dir) {
	auto *button = MainWindow::ui->mainButton;
	if (dir.isEmpty()) {
		button->setDisabled(true);
		MainWindow::ui->statusLabel->setText("No directory selected");
	} else if (QDir(dir).exists()) {
		button->setDisabled(false);
		MainWindow::ui->statusLabel->setText("Ready");
	} else {
		button->setDisabled(true);
		MainWindow::ui->statusLabel->setText("Couldn't open directory");
	}
}

void MainWindow::begin() {
	images.clear();
	image_size = 0;

	// change main button role
	ui->mainButton->setEnabled(false);
	ui->mainButton->setText("Cancel");
	disconnect(ui->mainButton, &QPushButton::clicked, this, &MainWindow::begin);
	connect(ui->mainButton, &QPushButton::clicked, &watcher, &QFutureWatcher<void>::cancel);

	// find supported extensions
	// 1. build list of all extensions
	std::vector<QString> exts;
	std::vector<QString> unsupported_exts;

	for (auto &o: optimisers) {
		if (o.available) {
			exts.insert(exts.end(), o.exts.begin(), o.exts.end());
		} else {
			unsupported_exts.insert(unsupported_exts.end(), o.exts.begin(), o.exts.end());
		}
	}

	unsupported_exts = fplus::keep_if([&](auto ext) {
		return exts.end() == std::ranges::find(exts, ext);
	}, unsupported_exts);

	// 2. if there are any unsupported extensions...
	if (!unsupported_exts.empty()) {
		// 2a. warn the user about the missing optimisers
		QStringList unsupported_optimisers;
		for (const auto &o: optimisers) {
			if (!o.available) {
				unsupported_optimisers.push_back(o.name);
			}
		}
		// 2b. if there are no optimisers whatsoever, we can't continue
		if (fplus::none_by([](const auto &o) { return o.available; }, optimisers)) {
			QString msg_text = "No supported optimisers were found. Please install at least one of the following programs, "
			                   "and ensure it is available on your system path:\n- "
				+ unsupported_optimisers.join("\n- ");

			QMessageBox msg(
				QMessageBox::Icon::Critical,
				"Error",
				msg_text,
				QMessageBox::StandardButton::Ok
			);
			msg.exec();

			resetState(true);
			return;
		}

		QStringList ext_list;
		for (const auto &ext: unsupported_exts) {
			ext_list.append(ext);
		}

		QString msg_text = "The following image optimisers were not found:\n- "
			+ unsupported_optimisers.join("\n- ")
			+ "\nAs a result, images with the following file extensions will not be optimised:\n ."
			+ ext_list.join(", .");

		QMessageBox msg(
			QMessageBox::Icon::Warning,
			"Warning",
			msg_text,
			QMessageBox::StandardButton::Ok | QMessageBox::StandardButton::Cancel
		);
		if (msg.exec() == QMessageBox::StandardButton::Cancel) {
			resetState(true);
			return;
		}
	}

	// filter to supported extensions
	QStringList exts_filter {};
	for (const auto &ext: exts) {
		exts_filter << "*." + ext;
	}
	qDebug() << "Supported extensions: " << exts_filter;

	// clear status messages
	MainWindow::ui->statusTextBox->clear();

	QDirIterator
		iter(MainWindow::ui->dirTxt->text(), exts_filter, QDir::Files, QDirIterator::Subdirectories);
	while (iter.hasNext()) {
		// add to images vector
		auto image = ImageInfo {
			.file = QFileInfo(iter.next()),
			.hash = 0,
			.status = ImageStatus::Pending
		};
		images.append(image);
	}

	if (images.length() == 0) {
		QMessageBox msg(
			QMessageBox::Icon::Information,
			"Information",
			"No supported image files were found in the given directory.",
			QMessageBox::StandardButton::Ok
		);
		msg.exec();
		resetState(true);
		return;
	}

	// set program status
	MainWindow::ui->statusLabel->setText(fmt::format("Hashing {} images...", images.length()).c_str());
	MainWindow::ui->progressBar->setRange(0, images.length());
	MainWindow::ui->progressBar->setTextVisible(true);
	MainWindow::ui->statusTextBox->appendPlainText("Computing image hashes");

	// connect slots
	connect(&watcher, &QFutureWatcher<void>::finished, this, &MainWindow::initialHashingComplete);
	connect(&watcher, &QFutureWatcher<void>::canceled, this, &MainWindow::initialHashingComplete);

	// calculate hash of all supplied images
	QFuture<void> future = QtConcurrent::map(images, updateImageInfo);
	watcher.setFuture(future);
	ui->mainButton->setEnabled(true);
}

void MainWindow::initialHashingComplete() {
	// disconnect watchers
	disconnect(&watcher, &QFutureWatcher<void>::finished, this, &MainWindow::initialHashingComplete);
	disconnect(&watcher, &QFutureWatcher<void>::canceled, this, &MainWindow::initialHashingComplete);

	ui->mainButton->setEnabled(false);

	// handle cancellation
	if (checkForCancellation()) {
		return;
	}

	QString status;
	auto prev_len = images.length();

	// remove any images that couldn't be opened
	images = fplus::keep_if([](const ImageInfo &i) {
		return i.hash != 0;
	}, images);

	if (prev_len != images.length()) {
		status.append(fmt::format("Skipping {} images that failed to open", prev_len - images.length()).c_str());
	}

	// remove any images that have already been processed
	prev_len = images.length();

	// this (potentially big) database lookup stuff happens synchronously, which, y'know, isn't great
	auto db = connectToDatabase();
	images = fplus::keep_if([&](const ImageInfo &i) {
		auto query = SQLite::Statement(
			db,
			"SELECT COUNT(*) FROM 'images' WHERE `dir` = ? AND `name` = ? AND `hash` = ?"
		);
		query.bind(1, i.file.dir().dirName().toStdString());
		query.bind(2, i.file.fileName().toStdString());
		query.bind(3, QString::number(i.hash, 16).toStdString());
		query.executeStep();
		return query.getColumn(0).getInt() == 0;
	}, images);

	if (prev_len != images.length()) {
		status.append(
			"Skipping " + QString::number(prev_len - images.length()) + " images that have already been optimised\n"
		);
	}

	// update size
	image_size = fplus::fold_left(
		[](uint64_t acc, const ImageInfo &i) { return acc + i.size; },
		(long double) 0.0,
		images
	);

	// qInfo() << "Ready to process " << fmt::format("{:.2f}", image_size).c_str() << "KiB";
	status.append(fmt::format("Ready to process {} images ({:.2f} KiB)\n", images.length(), image_size).c_str());

	//	for (auto image : images) {
	//		status.append(image.file.fileName() + '\t' + QString::number(image.hash, 16) + '\n');
	//	}

	// optimise images
	status.append("Optimising images");

	MainWindow::ui->statusTextBox->appendPlainText(status);

	MainWindow::ui->statusLabel->setText(fmt::format("Optimising {} images...", images.length()).c_str());
	MainWindow::ui->progressBar->setRange(0, images.length());

	QFuture<void> future = QtConcurrent::map(
		images,
		[this](ImageInfo &image) {
			// select ideal optimiser
			auto optimiser = fplus::find_first_by(
				[image](const Optimiser &o) {
					return o.exts.contains(image.file.completeSuffix().toLower());
				},
				optimisers
			);

			if (optimiser.is_nothing()) {
				// no optimiser available for given extension!
				// this shouldn't happen
				image.status = ImageStatus::Failed;
				return;
			}
			// otherwise, found an optimiser to use
			if (optimiseImage(image, optimiser.unsafe_get_just())) {
				// mark image as optimised
				image.status = ImageStatus::Success;
			} else {
				// oh no :c
				image.status = ImageStatus::Failed;
			}

			// update status textbox
			emit MainWindow::broadcastStatusUpdate(image.file.canonicalFilePath());
		}
	);
	connect(&watcher, &QFutureWatcher<void>::finished, this, &MainWindow::optimisingComplete);
	connect(&watcher, &QFutureWatcher<void>::canceled, this, &MainWindow::optimisingComplete);
	ui->mainButton->setEnabled(true);
	watcher.setFuture(future);
}

void MainWindow::optimisingComplete() {
	// disconnect watcher
	disconnect(&watcher, &QFutureWatcher<void>::finished, this, &MainWindow::optimisingComplete);
	disconnect(&watcher, &QFutureWatcher<void>::canceled, this, &MainWindow::optimisingComplete);

	ui->mainButton->setEnabled(false);

	// handle cancellation
	if (checkForCancellation()) {
		return;
	}

	// recompute image hashes
	MainWindow::ui->statusLabel->setText(fmt::format("Hashing {} images...", images.length()).c_str());
	MainWindow::ui->progressBar->setRange(0, images.length());
	MainWindow::ui->progressBar->setTextVisible(true);
	MainWindow::ui->statusTextBox->appendPlainText("Recomputing image hashes");

	// remove images where optimising failed
	auto prev_len = images.length();
	images = fplus::keep_if([](const auto &i) {
		return i.status != ImageStatus::Failed;
	}, images);


	if (prev_len != images.length()) {
		MainWindow::ui->statusTextBox->appendPlainText(
			fmt::format("Skipping {} images that failed to optimise", prev_len - images.length()).c_str()
		);
	}

	// calculate hash of all remaining images
	connect(&watcher, &QFutureWatcher<void>::finished, this, &MainWindow::finalHashingComplete);
	connect(&watcher, &QFutureWatcher<void>::canceled, this, &MainWindow::finalHashingComplete);
	QFuture<void> future = QtConcurrent::map(images, updateImageInfo);
	watcher.setFuture(future);
	ui->mainButton->setEnabled(true);
}

void MainWindow::finalHashingComplete() {
	// disconnect watcher
	disconnect(&watcher, &QFutureWatcher<void>::finished, this, &MainWindow::finalHashingComplete);
	disconnect(&watcher, &QFutureWatcher<void>::canceled, this, &MainWindow::finalHashingComplete);

	ui->mainButton->setEnabled(false);

	// handle cancellation
	if (checkForCancellation()) {
		return;
	}

	// update database
	auto db = connectToDatabase();
	SQLite::Statement(db, "BEGIN TRANSACTION").exec();

	for (const auto &i: images) {
		if (i.hash == 0) { continue; }
		auto dir = i.file.dir().dirName().toStdString();
		auto name = i.file.fileName().toStdString();
		auto hash = QString::number(i.hash, 16).toStdString();

		// try updating the existing entry, if any
		auto update = SQLite::Statement(db, "UPDATE 'images' SET `hash` = ? WHERE `dir` = ? AND `name` = ?");
		update.bind(1, hash);
		update.bind(2, dir);
		update.bind(3, name);
		auto rows = update.exec();

		if (rows == 0) {
			// no rows were affected by the statement - there must not be an existing entry for this file, so create a new one
			auto insert = SQLite::Statement(db, "INSERT INTO 'images' ('dir', 'name', 'hash') VALUES (?, ?, ?)");
			insert.bind(1, dir);
			insert.bind(2, name);
			insert.bind(3, hash);
			insert.exec();
		}
	}

	// commit transaction
	SQLite::Statement(db, "COMMIT").exec();

	auto new_image_size = fplus::fold_left(
		[](uint64_t acc, const ImageInfo &i) { return acc + i.size; },
		(long double) 0.0,
		images
	);

	MainWindow::ui->statusTextBox->appendPlainText(
		fmt::format("Processed {} images ({:.2f} KiB)", images.length(), new_image_size).c_str()
	);

	std::string result;
	if (std::abs(new_image_size - image_size) < 0.001) {
		result = "File sizes did not change.";
	} else {
		result = fmt::format(
			"{} {:.2f} KiB.",
			new_image_size < image_size ? "Saved" : "Wasted",
			std::abs(image_size - new_image_size)
		);
	}

	QMessageBox msg(
		QMessageBox::Information,
		"Information",
		fmt::format("Process complete. {}", result).c_str(),
		QMessageBox::StandardButton::Ok
	);
	msg.exec();

	resetState(false);
}

void MainWindow::resetState(bool clear_status) {
	// reset state
	MainWindow::ui->mainButton->setEnabled(false);
	MainWindow::ui->mainButton->setText("Optimise");
	MainWindow::ui->progressBar->setRange(0, 1);
	MainWindow::ui->progressBar->setTextVisible(false);
	MainWindow::ui->statusLabel->setText("No directory set");
	if (clear_status) { MainWindow::ui->statusTextBox->clear(); }

	// reset main button role
	ui->mainButton->setEnabled(true);
	ui->mainButton->setText("Begin");
	connect(ui->mainButton, &QPushButton::clicked, this, &MainWindow::begin);
	disconnect(ui->mainButton, &QPushButton::clicked, &watcher, &QFutureWatcher<void>::cancel);

	// check to see if directory in textbox is valid
	directorySelected(MainWindow::ui->dirTxt->text());
}

void MainWindow::updateImageInfo(ImageInfo &image) {
	// open file
	auto file = QFile(image.file.filePath());
	image.size = (long double) file.size() / 1024.0; // in kibibytes

	auto success = file.open(QIODevice::ReadOnly);
	if (!success) {
		// set hash to the sentinel value of zero to indicate it couldn't be opened -- i'm pretty sure xxhash doesn't
		// output zero...
		image.hash = 0;
		return;
	}

	// get file hash
	auto data = file.readAll().toStdString();
	auto hash = xxh::xxhash3<64>(data);
	image.hash = hash;

	// close file
	file.close();
}

bool MainWindow::optimiseImage(const ImageInfo &image, const Optimiser &optimiser) {
	QProcess task;
	QStringList args;

	// build arguments
	for (const auto &arg: optimiser.args) {
		if (arg == "") {
			args << image.file.canonicalFilePath();
		} else {
			args << arg;
		}
	}

	qInfo() << optimiser.bin << args;

	task.start(optimiser.bin, args, QIODevice::ReadOnly);
	task.waitForFinished(-1);
	return task.exitCode() == 0;
}

void MainWindow::handleStatusUpdate(const QString &text) {
	MainWindow::ui->statusTextBox->appendPlainText(text);
}

bool MainWindow::checkForCancellation() {
	if (watcher.isCanceled()) {
		connect(&watcher, &QFutureWatcher<void>::finished, this, &MainWindow::handleCancellation);

		ui->statusLabel->setText("Cancelling...");
		ui->progressBar->setRange(0, 0);
		ui->mainButton->setEnabled(false);
	}

	return watcher.isCanceled();
}

void MainWindow::handleCancellation() {
	disconnect(&watcher, &QFutureWatcher<void>::finished, this, &MainWindow::handleCancellation);
	ui->statusTextBox->appendPlainText("Canceled");
	resetState(false);
}

void MainWindow::initialiseOptimisers() {
	// TODO:
	// - locate tools on path
	// - store user-provided tool location

	// ordered roughly in order of preference
	optimisers.clear();
	optimisers.append(Optimiser {
		.name = "OxiPNG",
		.bin = "oxipng",
		.exts = {"png"},
		.args = {"-o", "COMPRESSION", "--preserve", "METADATA", "all", ""},
		.compression_levels = {"1", "2", "5", "max"},
		.metadata = {"--strip", "--keep"},
		.available = false,
	});
	optimisers.append(Optimiser {
		.name = "OptiPNG",
		.bin = "optipng",
		.exts = {"png"},
		.args = {"COMPRESSION", "-preserve", "METADATA", ""},
		.compression_levels = {"-o0", "-o1", "-o2", "-o5"},
		.metadata = {"-strip all", ""},
		.available = false,
	});
	optimisers.append(Optimiser {
		.name = "WEBP Compressor",
		.bin = "cwebp",
		.exts = {"webp"},
		.args = {"-mt", "-z", "COMPRESSION", "-metadata", "METADATA", "", "-o", ""},
		.compression_levels = {"3", "5", "7", "9"},
		.metadata = {"none", "all"},
		.available = false
	});
	optimisers.append(Optimiser {
		.name = "JPEGOptim",
		.bin = "jpegoptim",
		.exts = {"jpg", "jpeg", "jpe"},
		.args = {"-p", "-P", "METADATA", ""},
		.metadata = {"--strip-none", "--strip-all"},
		.available = false
	});
	optimisers.append(Optimiser {
		.name = "JPEGTran",
		.bin = "jpegtran",
		.exts = {"jpg", "jpeg", "jpe"},
		.args = {"-optimize", ""},
		.available = false
	});
	optimisers.append(Optimiser {
		.name = "Efficient Compression Tool",
		.bin = "ect",
		.exts = {"png", "jpeg", "jpg", "jpe"},
		.args = {"COMPRESSION", "METADATA", "--mt-deflate", "--strict", ""},
		.compression_levels = {"-3", "-5", "-7", "-9"},
		.metadata = {"-strip", "-keep"},
		.available = false
	});
	optimisers.append(Optimiser {
		.name = "Gifsicle",
		.bin = "gifsicle",
		.exts = {"gif"},
		.args = {"-w", "COMPRESSION", "", "-o", ""},
		.compression_levels = {"-O1", "-O2", "-O3", "-O3"},
		.available = false
	});

	// determine availability
	auto future = QtConcurrent::map(optimisers, [](auto &o) {
		// is the given optimiser available (i.e. on the $PATH)?
		auto test = QProcess();
		QStringList args = {};
		if (o.bin == "gifsicle") {
			// special case: if run without arguments, gifsicle will wait and try to read stdin
			args << "--version";
		}
		test.start(o.bin, args, QIODevice::ReadOnly);
		auto started = test.waitForStarted(2000);
		if (!test.waitForFinished(2000)) {
			// terminate still-running program after two seconds of waiting
			test.kill();
		}
		o.available = started;
	});
	future.waitForFinished();
}

void MainWindow::loadSettings() {
	using namespace matchit; // i know i know

	auto settings = QSettings("Lynnear Software", "SmallGoo");
	auto compression = settings.value("compression", QVariant::fromValue(Compression::High));
	auto keep_metadata = settings.value("keepMetadata", QVariant::fromValue(true));

	// configure optimisers according to retrieved settings values
	for (auto &o: optimisers) {
		for (auto &arg: o.args) {
			match(arg)(
				pattern | "COMPRESSION" = [&] { arg = o.compression_levels[compression.toInt()]; },
				pattern | "METADATA" = [&] { arg = o.metadata[keep_metadata.toInt()]; },
				pattern | _ = [&] { /* do nothing */ }
			);
		}
	}
}

SQLite::Database MainWindow::connectToDatabase() {
	auto data_path = QDir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
	if (!data_path.exists()) {
		// create config directory
		QDir().mkpath(data_path.absolutePath());
	}

	auto db_path = std::filesystem::path(data_path.absoluteFilePath("images.db").toStdString());
	auto db = SQLite::Database(db_path, SQLite::OPEN_CREATE | SQLite::OPEN_READWRITE);
	auto result = db.execAndGet(
		"SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='images';"
	).getInt();

	if (result == 0) {
		// create table
		// hashes are stored as strings because sqlite doesn't support 64 bit unsigned ints, and also because it's more
		// human-readable this way c:
		db.exec("CREATE TABLE 'images' ('dir' TEXT NOT NULL, 'name' TEXT NOT NULL, 'hash' TEXT NOT NULL);");
	}

	// vacuum database (https://www.sqlite.org/lang_vacuum.html)
	db.exec("VACUUM;");

	return db;
}

#pragma clang diagnostic pop
