#include "aboutdialogue.h"
#include "ui_aboutdialogue.h"

#include <QDesktopServices>
#include <QUrl>

AboutDialogue::AboutDialogue(QWidget *parent):
	QDialog(parent),
	ui(new Ui::AboutDialogue) {
	ui->setupUi(this);
}

AboutDialogue::~AboutDialogue() {
	delete ui;
}

void AboutDialogue::on_sourceCodeButton_clicked() {
	QDesktopServices::openUrl(QUrl("https://gitlab.com/Lynnesbian/smallgoo"));
}
