#ifndef IMAGEINFO_H
#define IMAGEINFO_H

#include <QFileInfo>
#include "xxhash.hpp"

enum class ImageStatus {
	Pending,
	Failed,
	Success,
	AlreadyOptimised
};

struct ImageInfo {
	QFileInfo file;
	xxh::hash_t<64> hash;
	ImageStatus status;
	long double size;
};

#endif // IMAGEINFO_H
