#ifndef OPTIMISER_H
#define OPTIMISER_H

#include <QStringList>

struct Optimiser {
	QString name;
	QString bin;
	QStringList exts;
	QStringList args;
	QVector<QString> compression_levels;
	QVector<QString> metadata;
	bool available;
};

#endif // OPTIMISER_H
