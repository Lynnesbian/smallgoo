//
// Created by lynne on 1/10/22.
//

#ifndef SMALLGOO_ABOUTDIALOGUE_H
#define SMALLGOO_ABOUTDIALOGUE_H


#include <QDialog>

namespace Ui {
	class AboutDialogue;
}

class AboutDialogue: public QDialog {
Q_OBJECT

public:
	explicit AboutDialogue(QWidget *parent = nullptr);

	~AboutDialogue();

//private slots:
//	void on_aboutButton_clicked();

private slots:
	void on_sourceCodeButton_clicked();

private:
	Ui::AboutDialogue *ui;
};


#endif //SMALLGOO_ABOUTDIALOGUE_H
