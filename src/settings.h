#ifndef SMALLGOO_SETTINGS_H
#define SMALLGOO_SETTINGS_H

#include <QVariant>

namespace Compression {
	enum Compression {
		Low,
		Medium,
		High,
		Ultra
	};

}
Q_DECLARE_METATYPE(enum Compression::Compression);
#endif //SMALLGOO_SETTINGS_H
