#include "settingswindow.h"
#include "ui_settingswindow.h"
#include "aboutdialogue.h"
#include "settings.h"

#include <QMessageBox>
#include "mainwindow.h"
#include "fmt/format.h"


#include <matchit.h>


SettingsWindow::SettingsWindow(QWidget *parent):
	QDialog(parent),
	ui(new Ui::SettingsWindow) {
	auto settings = QSettings("Lynnear Software", "SmallGoo");
	auto compression = settings.value("compression", QVariant::fromValue(Compression::High));
	auto keep_metadata = settings.value("keepMetadata", QVariant::fromValue(true));

	ui->setupUi(this);

	// connect signals to relevant slots
	connect(ui->aboutQtButton, &QAbstractButton::clicked, this, &QApplication::aboutQt);
	connect(ui->aboutButton, &QAbstractButton::clicked, this, &SettingsWindow::showAboutDialogue);
	connect(ui->clearDatabaseButton, &QAbstractButton::clicked, this, &SettingsWindow::clearDatabase);
	connect(ui->buttonBox, &QDialogButtonBox::clicked, this, &SettingsWindow::handleButtonBox);

	ui->compressionDropdown->setCurrentIndex(compression.toInt());
	ui->keepMetadataRadio->setChecked(keep_metadata.toBool());
	ui->discardMetadataRadio->setChecked(!keep_metadata.toBool());
}

SettingsWindow::~SettingsWindow() {
	delete ui;
}

void SettingsWindow::showAboutDialogue() {
	auto about = AboutDialogue();
	about.exec();
}


void SettingsWindow::clearDatabase() {
	auto db = MainWindow::connectToDatabase();
	auto rows = db.execAndGet("SELECT COUNT(*) FROM `images`").getInt();
	QMessageBox msg(
		QMessageBox::Icon::Question,
		"Question",
		fmt::format(
			"Are you sure you want to clear the image database? It currently contains {} entries.",
			rows
		).c_str(),
		QMessageBox::StandardButton::Yes | QMessageBox::StandardButton::No
	);

	if (msg.exec() == QMessageBox::StandardButton::Yes) {
		db.exec("DROP TABLE `images`");
	}
}


void SettingsWindow::handleButtonBox(QAbstractButton *button) {
	using namespace matchit;

	auto buttonBox = ui->buttonBox;

	match(button)(
		pattern | buttonBox->button(QDialogButtonBox::Apply) = [&] { saveSettings(); },
		pattern | buttonBox->button(QDialogButtonBox::Save) = [&] {
			saveSettings();
			this->close();
		},
		pattern | buttonBox->button(QDialogButtonBox::RestoreDefaults) = [&] { /* restore default settings */ },
		pattern | buttonBox->button(QDialogButtonBox::Cancel) = [&] { this->close(); }
	);
}

void SettingsWindow::saveSettings() {
	auto settings = QSettings("Lynnear Software", "SmallGoo");

	settings.setValue("compression", ui->compressionDropdown->currentIndex());
	settings.setValue("metadata", ui->keepMetadataRadio->isChecked());
	settings.sync();
}
