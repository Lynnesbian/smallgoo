#!/usr/bin/env sh
set -e

cd cmake-build-debug
cmake ..
cd ..
cmake --build ./cmake-build-debug
./cmake-build-debug/bin/SmallGoo

