SmallGoo
===
<div align="center">
![Screenshot](/doc/screenshot.png "Screenshot")

*A GUI frontend to various image optimisers. Written in C++ with [Qt].*
</div>

## Features
- Lossless image compression using various free and open source image optimisers
- Parallel processing
- Intuitive GUI
- Skips images that have already been optimised
- Supports PNG, JPEG, WEBP, and GIF files

## Building
After cloning this repository, `cd` into the resulting directory and execute the following:
```bash
cmake -B build -S .
cd build
make -j$(nproc)
```

The resulting binary should be found in the `bin` subdirectory.

Alternatively, just open the project with [Qt Creator](https://www.qt.io/product/development-tools). SmallGoo was 
developed using a combination of CLion and Qt Creator 7 on Fedora Linux 36.

## Dependencies
In order to do anything, SmallGoo requires that you have at least one supported optimiser installed. For example, to
optimise PNG files, you should have one of the supported PNG optimisers (OxiPNG, OptiPNG, Efficient Compression Tool...)
installed and on your `$PATH`.

In addition to the library dependencies listed below, you should also ensure that you have a working C++ compiler 
that is compatible with the C++20 standard, such as Clang, as well as git, the CMake build system, and Make (or another 
build tool, such as Ninja).
### Fedora
`sudo dnf install qt6-qtbase-devel qt6-qtbase sqlite-devel`
### Debian/Ubuntu
`sudo apt install qt6-base-dev libsqlite3-dev`
### Others
Coming soon ;)

## Attribution
This program uses the following libraries:
- [Qt], by The Qt Company - [LGPL 3.0]
  - *Note: Qt is not supplied with this program, and must be installed on the host system.*
- [SQLite](https://www.sqlite.org/index.html), by D. Richard Hipp - [Public Domain]
  - *Note: SQLite is not supplied with this program, and must be installed on the host system.*
- [xxhash_cpp](https://github.com/RedSpah/xxhash_cpp), by RedSpah - [BSD 2-Clause License]
- [FunctionalPlus](https://github.com/Dobiasd/FunctionalPlus), by Dobiasd - [Boost Software License]
- [SQLiteCpp](https://github.com/SRombauts/SQLiteCpp), by SRombauts - [MIT License]
- [{fmt}](https://fmt.dev/), by fmtlib - [MIT License]

## License
This program is licensed under the GNU [General Public License 3.0](https://www.gnu.org/licenses/gpl-3.0.en.html) or
later, at the user's discretion.

<pre>
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
</pre>

<!-- links -->
[Qt]: https://qt.io

[LGPL 3.0]: https://www.gnu.org/licenses/lgpl-3.0.en.html
[Public Domain]: https://creativecommons.org/share-your-work/public-domain/pdm/
[BSD 2-Clause License]: https://opensource.org/licenses/BSD-2-Clause
[Boost Software License]: https://www.boost.org/users/license.html
[MIT License]: https://opensource.org/licenses/MIT
